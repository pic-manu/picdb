#! /usr/bin/python3

#
# This file is part of the picdb software: 
#
# (c) Emmanuel Courcelle, emmanuel.courcelle@zaclys.net
#     et Projet Internet et Citoyenneté https://le-pic.org
#
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code.
#

import os
import sys
import traceback
import re
import requests
import csv
from requests.exceptions import HTTPError
import pprint

from cache import Cache
from conf import Config  # !!! Le config de ncags !!!
from affiche import *

def Usage():
    
    print("ENVIRONNEMENT")
    print("=============")
    print("export PICDB_URL=https://dolibarr.monasso.fr  L'url de base")
    print("export PICDB_KEY=abcdefgh                     Votre DOLAPIKEY, générée par Dolibarr")
    print("picdb_nca verif                               Signale les incohérences entre les sites existants sur pic2s et les données de Dolibarr")
    print()

def printNoms(noms: dict):
    '''noms est un dict, key = code client, val = liste de noms.
       Imprime tout ça'''
    
    # On imprime la liste des codes client et des comptes nc
    for k in sorted(noms):
        nom = noms[k]
        for n in sorted(nom):
            print(f"{k:10}{n}")
            k = ' '

def diffNoms(noms1:dict, noms2:dict) -> dict:
    '''noms1 et noms2 sont des dict - key = code client, val = liste de fqdn
       Retourne les noms présents dans noms1 et pas dans noms2'''

    noms1_2 = {}
    for k in sorted(noms1):
        if not noms2.get(k):
            noms1_2[k] = noms1[k]
        else:
            n1 = set(noms1[k])
            n2 = set(noms2[k])
            d = n1.difference(n2)
            if len(d) > 0:
                noms1_2[k] = d
    return noms1_2

def initCloud() -> (dict, int, int, int):
    '''Recherche les assos déclarées sur le cloud en utilisant l'objet 'Cache' issu de ncags
       Renvoie un dict: k -> set des noms (normalement un seul !) + qq stats '''
    
    # On collecte qq infos sur les clients puis on les imprime
    nb_assos = 0
    assos = {}

    cache = Cache()
    if not cache.readFromCache():
        raise Exception ("ERREUR - Le cache nca est inexistant ou trop ancien. Utilisez la commande nca pour le rafraichir")
    
    users = cache.users
    groups = cache.groups

    conf = Config()
    blacklist = conf.getConf('BLACKLIST')
    modele = re.compile('\W(A\d{4})\W')
   
    # Pour contruire les assos on prend le même algo que Cloud::__buildAssos dans ncags
    for ug in cache.groups:
        if not ug in cache.users:
            continue
        else:
            if not ug in blacklist:
                name = cache.users[ug].getName()
                code = modele.search(' ' + name + ' ')
                if code != None:
                    k = code.group(1)
                    if k == None:
                        k = 'E0000'
                else:
                    k = 'A0000'
                
                # normalement 1 seul nom par assos !
                if not k in assos:
                    assos[k] = set()
                    nb_assos += 1
                assos[k].add(ug)    
            
        
    # Retirer les pseudo-asso A0000 et E0000
    if 'A0000' in assos:
        nb_assos -= 1
    if 'E0000' in assos:
        nb_assos -= 1

    return (assos, 0, 0, nb_assos)

def initDolib(dolikey:str, baseurl:str) -> (dict, int, int, int):
    '''Utilise l'API dolibarr pour récupérer la liste des clients
       Renvoie:
        - un dict A0123 -> [noms] 
        - Quelques stats
       Peut imprimer des warnings !'''

    headers = { 'Accept': 'application/json', 'DOLAPIKEY': dolikey }
    
    # On fait la liste des tiers, clients associations seulement
    url = baseurl + '/api/index.php/thirdparties'
    params = { 'sortfield':'t.code_client', 'sortorder': 'ASC', 'limit': 2000 }
    
    # On lance la requête pour voir la liste des clients
    req = requests.get(url, params=params, headers=headers)
    req.raise_for_status()
    clients = req.json()
    
    f_flag = 'options_cloud'
    f_nom = 'options_cloudnom'

    # On collecte qq infos sur les clients puis on les imprime
    dolib_assos = 0
    dolib_assos_noms = 0
    dolib_nb_noms = 0
    dolib_noms = {}

    for c in clients:
        if c['client'] == '1' and c['status'] == '1' and c['typent_code'] == 'TE_ASSADH':
            dolib_assos += 1
            if c['array_options'][f_flag] != None:
                dolib_assos_noms += 1
                try:
                    noms = sorted(c['array_options'][f_nom].split(','))
                    noms = [ n.strip() for n in noms ]
                    dolib_noms[c['code_client']] = noms
                    dolib_nb_noms += len(noms)
                except:
                    print (afficheJaune(f"ATTENTION - {c['code_client']} données incohérentes"))

    return (dolib_noms, dolib_assos, dolib_assos_noms, dolib_nb_noms)
    
# =============================== m a i n ===========================================
if '--help' in sys.argv:
    Usage()
    exit(0)

if len(sys.argv) != 2 or (not sys.argv[1] in ['verif']):
    Usage()
    exit(1)

try:
    # On passe qq paramètres par l'environnement
    DOLAPIKEY = os.getenv('PICDB_KEY')
    IFKEY = os.getenv('PICDB_IFKEY')
    BASEURL = os.getenv('PICDB_URL')
    DOMAINES = os.getenv("PICDB_DOMAINES") 
       
    if DOLAPIKEY == None:
        raise Exception("ERREUR - la variable d'environnement PICDB_KEY n'est pas définie !")
    if BASEURL == None:
        raise Exception("ERREUR - la variable d'environnement PICDB_URL n'est pas définie !")
    if os.getenv('PYTHONPATH') == None:
        raise Exception("ERREUR - la variable d'environnement PYTHONPATH n'est pas définie !")

    print("=======================================")
    print(" LES ASSOS DU CLOUD")
    print("=======================================")
    
    # Serveur local: On vérifie les assos qui ont un compte sur le cloud
    (local_noms, dum1, dum2, local_nb_noms) = initCloud()
    printNoms(local_noms)
    
    print ("=========================")
    print (f"NOMBRE D'ASSOS  = {local_nb_noms}")

    print ()
    print("=====================================================")
    print(" LES ASSOS DECLAREES DANS DOLIBARR UTILISANT LE CLOUD")    
    print("=====================================================")

    # On lance la requête sur Dolibarr pour avoir la liste des noms de sites
    (dolib_noms, dolib_assos, dolib_assos_noms, dolib_nb_noms) = initDolib(dolikey=DOLAPIKEY, baseurl=BASEURL)

    # On imprime la liste des code_client et des sites
    printNoms(dolib_noms)

    print()
    print ("============================================")
    print (f"NOMBRE D'ASSOS  = {dolib_assos_noms}")
    print (f"NOMBRE DE SITES = {dolib_nb_noms}" )
    print (f"NB TOTAL D'ASSOS= {dolib_assos}")
    print ("============================================")

    print()
    print("===============================================================")
    print("LES ASSOS DECLAREES DANS DOLIBARR QUI NE SONT PAS DANS LE CLOUD") 
    print("===============================================================")
    printNoms(diffNoms(dolib_noms, local_noms))
    
    print()
    print("====================================================")
    print(" LES ASSOS DANS LE CLOUD NON DECLARES DANS DOLIBARR") 
    print("====================================================")
    printNoms(diffNoms(local_noms, dolib_noms))

except KeyboardInterrupt:
    print(afficheRouge("\n\n===> Action interrompue par un CTRL-C"))
        
except HTTPError as he:
    print(afficheRouge(f"Erreur HTTP: {he}"))
    if 'PICDB_DEBUG' in os.environ:
        traceback.print_exception(e)

except Exception as e:
    print(afficheRouge(f"Erreur: {e}"))
    if 'PICDB_DEBUG' in os.environ:
        traceback.print_exception(e)
    
else:
    print("OK")

