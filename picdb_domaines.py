#! /usr/bin/python3

#
# This file is part of the picdb software: 
#
# (c) Emmanuel Courcelle, emmanuel.courcelle@zaclys.net
#     et Projet Internet et Citoyenneté https://le-pic.org
#
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code.
#

import os
import sys
import traceback
import re
import requests
import csv
from requests.exceptions import HTTPError
import pprint
# from factures import *
from affiche import *

def Usage():
    
    print("ENVIRONNEMENT")
    print("=============")
    print("export PICDB_URL=https://dolibarr.monasso.fr        L'url de base")
    print("export PICDB_KEY=abcdefgh                           Votre DOLAPIKEY, générée par Dolibarr")
    print("export PICDB_IFKEY=ijklmn                           La clé générée par infomaniak")
    print("export PICDB_DOMAINES                               CHEMIN COMPLET du fichier généré par Gandi: Noms de domaines / Vue avancée / roue dentée / Exporter toutes les données en CSV")
    print("picdb_domaines verif [--help]")
    print("                  Signale les incohérences entre les domaines enregistrés chez Gandi et les domaines enregistrés dans Dolibarr")
    print()

def printDoms(domaines):
    '''domaines est un dict, key = code client, val = liste de fqdn.
       Imprime tout ça'''
    
    # On imprime la liste des codes client et des domaines
    for k in sorted(domaines):
        doms = domaines[k]
        for d in doms:
            print(f"{k:10}{d}")
            k = ' '

def diffDoms(domaines1, domaines2):
    '''domaines1 et domaines2 sont des dict - key = code client, val = liste de fqdn
       Retourne les domaines présents dans domaines1 et pas dans domaines2'''

    domaines1_2 = {}
    for k in sorted(domaines1):
        if not domaines2.get(k):
            domaines1_2[k] = domaines1[k]
        else:
            d1 = set(domaines1[k])
            d2 = set(domaines2[k])
            d_m = d1.difference(d2)
            if len(d_m) > 0:
                domaines1_2[k] = d_m
    return domaines1_2

def initGandiDoms(domaines_csv) -> dict:
    '''Lit le fichier csv domaines_csv, génère et renvoie le dictionnaire gandi_domaines
       key = code client, val = liste de fqdn '''
    re_cc = re.compile('(A\d\d\d\d)\W')
    gandi_domaines = {}
    with open(DOMAINES, newline='') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            # On ajoute un espace pour être sûr que le \w soit bien pris en compte
            tags = row['tags'] + ' '
            f = re_cc.search(tags)
            if f == None:
                cc = 'A0000'
            else:
                cc = f.group(1)
            if not gandi_domaines.get(cc):
                gandi_domaines[cc] = []
            gandi_domaines[cc].append(row['fqdn'])
            #print(f"{cc:10}{row['fqdn']:50}{row['tags']}")
    return gandi_domaines

def initIfkDoms(ifkey) -> dict:
    '''Utilise l'API infomaniak pour lire les noms de domaines, générer et renvoyer le dictionaire ifk_domaines
       key = code client, val = liste de fqdn '''
    url = "https://api.infomaniak.com/1/products"
    headers = {
      'Authorization': f'Bearer {ifkey}',
      'Content-Type': 'application/json',
    }
    req = requests.request("GET", url = url , headers = headers)
    req.raise_for_status()
    products = req.json()['data']
    
    ifk_domaines = {}
    re_cc = re.compile('(A\d\d\d\d)\W*')
    for p in products:
        if p['service_name'] == 'domain':
            customer_name = p['customer_name']
            # Recherche de Axyzt dans un tag
            cc = 'A0000'
            for t in p['tags']:
                f = re_cc.search(t['name'])
                if f != None:
                    cc = f.group(1)
                    break
            if not ifk_domaines.get(cc):
                ifk_domaines[cc] = []
            ifk_domaines[cc].append(customer_name)
                
    import pprint
    #pprint.pprint(products)
    #pprint.pprint(ifk_domaines)
    return ifk_domaines

def initDolib(dolikey, baseurl):
    '''Utilise l'API dolibarr pour récupérer la liste des clients'''

    headers = { 'Accept': 'application/json', 'DOLAPIKEY': dolikey }
    
    # On fait la liste des tiers, clients associations seulement
    url = baseurl + '/api/index.php/thirdparties'
    params = { 'sortfield':'t.code_client', 'sortorder': 'ASC', 'limit': 2000 }
    
    #F = Factures(BASEURL, H, COTIS, DOM, ANNEE)

    # On lance la requête pour voir la liste des clients
    req = requests.get(url, params=params, headers=headers)
    req.raise_for_status()
    clients = req.json()
    return clients
    #pprint.pprint(req.json())
    
if '--help' in sys.argv:
    Usage()
    exit(0)

if len(sys.argv) != 2 or (not sys.argv[1] in ['verif']):
    Usage()
    exit(1)

try:
    # On passe qq paramètres par l'environnement
    DOLAPIKEY = os.getenv('PICDB_KEY')
    IFKEY = os.getenv('PICDB_IFKEY')
    BASEURL = os.getenv('PICDB_URL')
    DOMAINES = os.getenv("PICDB_DOMAINES") 
       
    if DOLAPIKEY == None:
        raise Exception("ERREUR - la variable d'environnement PICDB_KEY n'est pas définie !")
    if BASEURL == None:
        raise Exception("ERREUR - la variable d'environnement PICDB_BASEURL n'est pas définie !")
    if DOMAINES == None:
        raise Exception("ERREUR - la variable d'environnement PICDB_DOMAINES n'est pas définie !")
    if IFKEY == None:
        raise Exception("ERREUR - la variable d'environnement PICDB_IFKEY n'est pas définie !")

    # On lance la requête sur Dolibarr pour avoir la liste des clients
    clients = initDolib(DOLAPIKEY, BASEURL)
    
    # Gandi: On collecte qq infos sur les domaines puis on les imprime
    gandi_domaines = initGandiDoms(DOMAINES)

    # infomaniak: On collecte qq infos sur les domaines puis on les imprime
    ifk_domaines = initIfkDoms(IFKEY)
    
    # On fusionne les deux dictionnaires
    gandi_domaines.update(ifk_domaines)
    
    print("=======================================")
    print(" LES DOMAINES GERES CHEZ GANDI/INFOMANIAK")    
    printDoms(gandi_domaines)

    # Dolibarr: On collecte qq infos sur les clients puis on les imprime
    ttl_nb_doms = 0
    dolib_assos = 0
    dolib_assos_doms = 0
    dolib_nb_doms = 0
    dolib_domaines = {}
    for c in clients:
        if c['client'] == '1' and c['status'] == '1' and c['typent_code'] == 'TE_ASSADH':
            dolib_assos += 1
            if c['array_options']['options_dom'] != None:
                dolib_assos_doms += 1
                doms = sorted(c['array_options']['options_domnom'].split(','))
                dolib_domaines[c['code_client']] = doms
                dolib_nb_doms += len(doms)

    print("=======================================")
    print(" LES DOMAINES DECLARES DANS DOLIBARR")    
    # On imprime la liste des code_client et des domaines
    printDoms(dolib_domaines)

    print ("=========================")
    print (f"NOMBRE D'ASSOS                     = {dolib_assos}")
    print (f"NOMBRE D'ASSOS AVEC DOMAINES GERES = {dolib_assos_doms}")
    print (f"NOMBRE DE DOMAINES                 = {dolib_nb_doms}" )

    # La différence !
    dolib_m_gandi = {}
    for k in sorted(dolib_domaines):
        if not gandi_domaines.get(k):
            dolib_m_gandi[k] = dolib_domaines[k]
        else:
            dd = set(dolib_domaines[k])
            dg = set(gandi_domaines[k])
            d_m = dd.difference(dg)
            if len(d_m) > 0:
                dolib_m_gandi[k] = d_m
                
    print("=======================================")
    print(" LES DOMAINES DECLARES DANS DOLIBARR et PAS CHEZ GANDI/INFOMANIAK:") 
    dolib_m_gandi = diffDoms(dolib_domaines, gandi_domaines)   
    printDoms(dolib_m_gandi)

    print("=======================================")
    print(" LES DOMAINES CHEZ GANDI/INFOMANIAK NON DECLARES DANS DOLIBARR:") 
    gandi_m_dolib = diffDoms(gandi_domaines, dolib_domaines)   
    printDoms(gandi_m_dolib)
                
except KeyboardInterrupt:
    print(afficheRouge("\n\n===> Action interrompue par un CTRL-C"))
        
except HTTPError as he:
    print(afficheRouge(f"Erreur HTTP: {he}"))
    if 'PICDB_DEBUG' in os.environ:
        traceback.print_exception(e)

except Exception as e:
    print(afficheRouge(f"Erreur: {e}"))
    if 'PICDB_DEBUG' in os.environ:
        traceback.print_exception(e)

    
else:
    print("OK")

