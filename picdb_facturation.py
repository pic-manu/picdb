#! /usr/bin/python3

#
# This file is part of the picdb software: 
#
# (c) Emmanuel Courcelle, emmanuel.courcelle@zaclys.net
#     et Projet Internet et Citoyenneté https://le-pic.org
#
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code.
#

import os
import sys
import traceback
import re
import requests
from requests.exceptions import HTTPError
import pprint
from factures import *
from affiche import *

#import re
#import time
#import socket
#import subprocess
#import traceback

def Usage():
    
    print("ENVIRONNEMENT")
    print("=============")
    print("export PICDB_URL=https://dolibarr.monasso.fr        L'url de base")
    print("export PICDB_KEY=abcdefgh                           Votre DOLAPIKEY, générée par Dolibarr")
    print("export PICDB_COTIS=Adh2024a                         La référence du 'service' cotisation (voir Dolibarr, Produits/Services")
    print("export PICDB_DOM=Dom2024                            La référence du 'service' domaines (voir Dolibarr, Produits/Services")
    print("export PICDB_ANNEE=2024                             L'année pour les cotisations et Domaines")
    print("picdb_facturation verif [--help]")
    print("                  Se contente d'afficher l'état des factures et recherche celles qui contiennent COTIS ")
    print()
    print("picdb_facturation emettre [--help]")
    print("                  Emet une facture pour chaque clent associatif, avec une ligne pour la cotisation et une ligne pour les domaines")
    print("                  Ne fait rien si la facture est déjà émise")
    print()
    print("picdb_facturation domaines [--help]")
    print("                  Affiche la liste des noms de domaines qui seront facturés")
    print()
    print("picdb_facturation suppr [--help]")
    print("                  SUPPRIME toutes les factures qui peuvent être supprimées - SEULEMENT POUR DEBUG !!!")

if '--help' in sys.argv:
    Usage()
    exit(0)

if len(sys.argv) != 2 or (not sys.argv[1] in ['verif', 'suppr', 'emettre', 'domaines']):
    Usage()
    exit(1)

try:
    # On passe qq paramètres par l'environnement
    DOLAPIKEY = os.getenv('PICDB_KEY')
    BASEURL = os.getenv('PICDB_URL')
    COTIS = os.getenv('PICDB_COTIS')
    DOM = os.getenv('PICDB_DOM')
    ANNEE = os.getenv('PICDB_ANNEE')
    
    if DOLAPIKEY == None:
        raise Exception("ERREUR - la variable d'environnement PICDB_KEY n'est pas définie !")
    if BASEURL == None:
        raise Exception("ERREUR - la variable d'environnement PICDB_BASEURL n'est pas définie !")
    if COTIS == None:
        raise Exception("ERREUR - la variable d'environnement PICDB_COTIS n'est pas définie !")
    if DOM == None:
        raise Exception("ERREUR - la variable d'environnement PICDB_DOM n'est pas définie !")
    if ANNEE == None:
        raise Exception("ERREUR - la variable d'environnement PICDB_ANNEE n'est pas définie !")
    if int(ANNEE) < 2020:
        raise Exception("ERREUR - la variable d'environnement PICDB_ANNEE doit être >= 2020 !")
    if sys.argv[1] == 'suppr':
        if re.search('-db[0-9]+',BASEURL)==None:
            raise Exception("ERREUR - Pas de suppr sur une instance de production !")
        
    # Headers génériques
    H = { 'Accept': 'application/json', 'DOLAPIKEY': DOLAPIKEY }
    
    # On fait la liste des tiers, clients associations seulement
    URL = BASEURL + '/api/index.php/thirdparties'
    P = { 'sortfield':'t.code_client', 'sortorder': 'ASC', 'limit': 2000 }
    
    F = Factures(BASEURL, H, COTIS, DOM, ANNEE)

    # Suppr - On supprime les factures en commençant par la dernière, indépendamment du tiers
    if sys.argv[1] == 'suppr':
        F.supprFactures()
        exit(0)

    # On lance la requête pour voir la liste des clients
    R = requests.get(URL, params=P, headers=H)
    R.raise_for_status()
    clients = R.json()
    #pprint.pprint(R.json())

    # On imprime qq infos sur les clients
    ttl_nb_doms = 0
    assos = 0
    for c in clients:
        if c['client'] == '1' and c['status'] == '1' and c['typent_code'] == 'TE_ASSADH':
            assos += 1
            if c['array_options']['options_dom'] != None:
                doms = c['array_options']['options_domnom'].split(',')
                nb_doms = len(doms)
                ttl_nb_doms += nb_doms
            else:
                doms = []
                nb_doms = 0

            # pour Debug
            # if c['code_client'] != 'A0132':
            #    continue

            # Vérifications - juste de l'affichage
            if sys.argv[1] == 'verif':
                fact = F.verifFactures(c)
                if len(fact['imp']) == 0:
                    msg = afficheVert(f"Pas de facture impayée")
                else:
                    refs = ','.join([ f['ref'] for f in fact['imp'] ])
                    msg = afficheJaune(f"Factures impayées: {refs}")
    
                if fact['fait'] == 0:
                    msg1 = afficheRouge(f"Facture pour {COTIS} pas émise")
                if fact['fait'] == 1:
                    msg1 = afficheJaune(f"Facture pour {COTIS} émise mais impayée")
                if fact['fait'] == 2:
                    msg1 = afficheVert(f"Facture pour {COTIS} émise et payée")
               
                #print (f"{c['name']} - {c['name_alias']} - code={c['code_client']} socid={c['id']} - {nb_doms} domaines - {msg} - {msg1}")
                print (f"{c['name']} - {c['name_alias']} - code={c['code_client']} - {nb_doms} domaines - {msg} - {msg1}")
                
            # Domaines - juste de l'affichage
            if sys.argv[1] == 'domaines':
                for d in doms:
                    print (d.strip())

            if sys.argv[1] == 'emettre':
                [emis, fact, ttal] = F.emettreFacture(c)
                if emis:
                    print (afficheVert(f"{c['name']} - {c['code_client']}: Facture émise {fact['ref']} -> {ttal}€"))
                    #break     # Pour debug
                else:
                    print (afficheJaune(f"{c['name']} - {c['code_client']}: La facture est déjà émise {fact['ref']}"))
    
    print ("=========================")
    print (f"NOMBRE D'ASSOS = {assos}")
    print (f"NOMBRE DE DOMAINES GERES PAR le-pic = {ttl_nb_doms}" )
    
except KeyboardInterrupt:
    print(afficheRouge("\n\n===> Action interrompue par un CTRL-C"))
        
except HTTPError as he:
    print(afficheRouge(f"Erreur HTTP: {he}"))
    if 'PICDB_DEBUG' in os.environ:
        traceback.print_exception(he)

except Exception as e:
    print(afficheRouge(f"Erreur: {e}"))
    if 'PICDB_DEBUG' in os.environ:
        traceback.print_exception(e)

    
else:
    print("OK")

