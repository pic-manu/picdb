#! /usr/bin/python

#
# This file is part of the picdb software: 
#
# (c) Emmanuel Courcelle, emmanuel.courcelle@zaclys.net
#     et Projet Internet et Citoyenneté https://le-pic.org
#
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code.
#

import requests
import pprint
import signal
from datetime import datetime
from affiche import *
from requests.exceptions import HTTPError

# Modèle pdf par défaut
# Pour la génération des factures par le gui, aller sur: Configuration / modules / Factures /Divers
# Pour la génération des factures par l'API, c'est ici !
DEF_MODEL_PDF = 'crobe'

class Factures:

    def __getProductByRef(self, ref):
        '''On lui passe une référence il renvoie un produit et un seul, ou alors lance une exception'''

        url = self.__baseurl + '/api/index.php/products'
        p = { 'sqlfilters' : f"(t.ref:=:'{ref}')" }
        h = self.__headers
        R = requests.get(url, params=p, headers=h)
        R.raise_for_status()
        products = R.json()
        if len(products) != 1:
            raise Exception(f"Pas de produit, ou plusieurs produits de référence {ref}: {len(products)}")
        return products[0]
            
    def __init__(self, baseurl, headers, cotis, dom, annee):
        ''' baseurl = L'url de base
            headers = les headers des requêtes http
            cotis = la référence dolibarr correspondant à la cotisation courante - ex. Adh2024a
            annee = L'année considérée, format 2024 '''
        self.__baseurl = baseurl
        self.__headers = headers
        self.__cotis = cotis
        self.__product_cotis = self.__getProductByRef(cotis)
        self.__product_dom = self.__getProductByRef(dom)
        self.__annee = annee
        self.__date_start = int(datetime.fromisoformat(f"{annee}-01-01").timestamp())
        self.__date_end = int(datetime.fromisoformat(f"{annee}-12-31").timestamp())

    def verifFactures(self, tiers):
        ''' Recherche les factures impayées (status=1)
            Renvoie les infos suivantes:
                imp = Les factures impayées
                fait = Si 0, la facture de l'année en cours n'a pas été émise
                       Si 1, la facture de l'année en cours a été émise, et est impayée
                       Si 2, la facture de l'année en cours a été émise, et est payée
                fact = Si fait = 1 ou 2, la facture COTIS, sinon None'''
                
        url = self.__baseurl +  '/api/index.php/invoices'
        p = { 'sortfield':'t.rowid', 'sortorder': 'DESC', 'thirdparty_ids': tiers['id'], 'limit': 2000 }
        h = self.__headers
        #print (f"koukou {url} {p}")
        try:
            R = requests.get(url, params=p, headers=h)
            R.raise_for_status()
        except HTTPError as e:
            # Pas de facture du tout ! (nouveau tiers)
            factures = []
        else:
            factures = R.json()
        
        # On compte le nombre de factures impayées et on cherche une facture correspondant à cotis
        fait = 0
        imp = []
        fact = None
        for facture in factures:
            status = facture['status'] 
            if status == '1':
                imp.append(facture)
            for ligne in facture["lines"]:
                if ligne["ref"] == self.__cotis:
                    fact = facture
                    if status == '1':
                        fait = 1
                    if status == '2':
                        fait = 2
                    
        return { 'fait': fait, 'imp': imp, 'fact' : fact}

    def supprFactures(self):
        ''' Recherche les factures qui contiennent une ligne COTIS et les supprime'''
                
        url = self.__baseurl + '/api/index.php/invoices'
        p = { 'sortfield':'t.rowid', 'sortorder': 'DESC', 'limit': 2000 }
        h = self.__headers
        R = requests.get(url, params=p, headers=h)
        R.raise_for_status()
        factures = R.json()
        
        # on cherche une facture correspondant à cotis pour la supprimer
        for facture in factures:
            
            for ligne in facture["lines"]:
                if ligne["ref"] == self.__cotis:
                    p = { 'id' : facture['id']}
                    R = requests.delete(url + '/' + facture['id'], headers=h)
                    if R:
                        print (afficheVert(f"La facture {facture['ref']} a été correctement supprimée"))
                    else:
                        print (afficheJaune(f"La facture {facture['ref']} n'a pas pu être supprimée"))

    def emettreFacture(self, tiers):
        ''' Si pas déjà fait, émet une facture pour COTIS
            Ajoute une ligne pour les domaines si nécessaire
            Return: [False, fact, ttal] -> La facture est déjà émise on ne fait rien
                    [True, fact, ttal]  -> La facture vient d'être créée'''
        
        v = self.verifFactures(tiers)
        if v['fait'] != 0:
            return [False, v['fact'],0]

        else:
            # Ignorer CTRL-C durant l'émission de la facture
            signal.signal(signal.SIGINT,signal.SIG_IGN)

            # Les factures impayées de l'année passée
            impayees = v['imp']
            
            # Création de la facture
            url = self.__baseurl + '/api/index.php/invoices'
            d = { 'socid' : tiers['id'], 'cond_reglement_id' : '2', 'model_pdf': DEF_MODEL_PDF, 'ref_client': tiers['code_client'], 'ref_customer': tiers['code_client'] }
            h = self.__headers
            R = requests.post(url, data=d, headers=h)
            R.raise_for_status()
            facid = R.json()
            
            # Ajout des lignes
            # 1/ Cotisation
            url = self.__baseurl + f'/api/index.php/invoices/{facid}/lines'
            pdt = self.__product_cotis
            d = { 'ref' : pdt['ref'], 
                  'qty' : 1, 
                  'subprice' : pdt['price'], 
                  'fk_product' : pdt['id'], 
                  'total_ht' : pdt['price'], 
                  'total_ttc' : pdt['price_ttc'], 
                  'product_label' : pdt['label'], 
                  'libelle' : pdt['label'],
                  'date_start' : self.__date_start,
                  'date_end' : self.__date_end}
            
            R = requests.post(url, headers=h, data=d)   
            R.raise_for_status()
            #print(facid)

            # Domaines
            #pprint.pprint(tiers['array_options'])
            if tiers['array_options']['options_dom'] != None:
                doms = tiers['array_options']['options_domnom']
                qty = len(doms.split(','))
                desc = doms.replace(',',"\n").replace(' ','')
                url = self.__baseurl + f'/api/index.php/invoices/{facid}/lines'
                pdt = self.__product_dom
                d = { 'ref' : pdt['ref'], 
                      'qty' : qty, 
                      'subprice' : pdt['price'], 
                      'fk_product' : pdt['id'], 
                      'total_ht' : pdt['price'], 
                      'total_ttc' : pdt['price_ttc'], 
                      'product_label' : pdt['label'], 
                      'libelle' : pdt['label'], 
                      'desc' : desc, 
                      'description' : desc,
                      'date_start' : self.__date_start,
                      'date_end' : self.__date_end}
                R = requests.post(url, headers=h, data=d)   
                R.raise_for_status()

            # Ajout des lignes de l'an passé
            if len(impayees) != 0:
                for fact_imp in impayees:
                    for ligne in fact_imp["lines"]:
                        R = requests.post(url, headers=h, data=ligne)
                        R.raise_for_status()
            
            # Validation de la facture
            url = self.__baseurl + f'/api/index.php/invoices/{facid}/validate'
            R = requests.post(url, headers=h)            
            R.raise_for_status()

            # Récupération de la facture qui vient d'être créée
            url = self.__baseurl + '/api/index.php/invoices'
            p = { 'id' : facid, 'contact_list' : 1 }
            R = requests.get(url + '/' + str(facid), headers=h, params=p)
            R.raise_for_status()
            f = R.json()
            ttal = int(float(f['total_ttc']))
            
            # Génération de la version PDF
            url = self.__baseurl + f'/api/index.php/documents/builddoc'
            p = { 'modulepart' : 'invoice', 'langcode' : 'fr_FR', 'original_file' : f"{f['ref']}/{f['ref']}.pdf"}
            R = requests.put(url, headers=h, params=p)
            R.raise_for_status()

            # Comportement par défaut pour CTRL-C
            signal.signal(signal.SIGINT,signal.SIG_DFL)

            return  [True, f, ttal]
