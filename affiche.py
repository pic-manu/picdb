#! /usr/bin/python

#
# This file is part of the picgs software
#
# (c) Emmanuel Courcelle, emmanuel.courcelle@zaclys.net
#     et Projet Internet et Citoyenneté https://le-pic.org
#
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code.
#

#
# MODULE affiche = Quelques fonctions d'affichage
#
import os;

def afficheRouge(msg):
    '''Renvoie pour affichage le message en gras+rouge'''
    if not os.getenv('PICGS_HTML'):
        return f"\033[31;1m{msg}\033[0m"
    else:
        return f'<span style="color:red;font-weight:bold">{msg}</span>'
    
def afficheJaune(msg):
    '''Renvoie pour affichage le message en gras+jaune'''
    if not os.getenv('PICGS_HTML'):
        return f"\033[33;1m{msg}\033[0m"
    else:
        return f'<span style="color:yellow;font-weight:bold">{msg}</span>'

def afficheVert(msg):
    '''Renvoie pour affichage le message en gras+vert'''
    if not os.getenv('PICGS_HTML'):
        return f"\033[32;1m{msg}\033[0m"
    else:
        return f'<span style="color:green;font-weight:bold">{msg}</span>'

def afficheGras(msg):
    '''Renvoie pour affichage le message en gras'''
    if not os.getenv('PICGS_HTML'):
        return f"\033[1m{msg}\033[0m"
    else:
        return f'<span style="font-weight:bold">{msg}</span>'
