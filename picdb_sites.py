#! /usr/bin/python3

#
# This file is part of the picdb software: 
#
# (c) Emmanuel Courcelle, emmanuel.courcelle@zaclys.net
#     et Projet Internet et Citoyenneté https://le-pic.org
#
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code.
#

import os
import sys
import traceback
import re
import requests
import csv
from requests.exceptions import HTTPError
import pprint
# from factures import *
from affiche import *
from picgsldap import *
from picconfig import getConf

def Usage():
    
    print("ENVIRONNEMENT")
    print("=============")
    print("export PICDB_URL=https://dolibarr.monasso.fr        L'url de base")
    print("export PICDB_KEY=abcdefgh                           Votre DOLAPIKEY, générée par Dolibarr")
    print("picdb_sites verif st|gt|db|au [--help]")
    print("                  Signale les incohérences entre les sites existnat sur pic2s et les données de Dolibarr")
    print()

def printNoms(noms: dict, codefct: str = ""):
    '''noms est un dict, key = code client, val = liste de noms.
       Imprime tout ça'''
    
    # On imprime la liste des codes client et des sites
    warning = afficheJaune(f"NE SE TERMINE PAS PAR {codefct}")
    for k in sorted(noms):
        nom = noms[k]
        for n in sorted(nom):
            if not n.endswith(codefct):
                w = warning
            else:
                w = " "
            print(f"{k:10}{n} {w}")
            k = ' '

def diffNoms(noms1:dict, noms2:dict) -> dict:
    '''noms1 et noms2 sont des dict - key = code client, val = liste de fqdn
       Retourne les noms présents dans noms1 et pas dans noms2'''

    noms1_2 = {}
    for k in sorted(noms1):
        if not noms2.get(k):
            noms1_2[k] = noms1[k]
        else:
            n1 = set(noms1[k])
            n2 = set(noms2[k])
            d = n1.difference(n2)
            if len(d) > 0:
                noms1_2[k] = d
    return noms1_2

def initSites(codefct:str) -> (dict, int, int, int):
    '''Recherche les sites 'houaibe' sur le serveur local
       Renvoie un dict: k -> set des noms courts + qq stats '''
    
    # On collecte qq infos sur les clients puis on les imprime
    assos_noms = 0
    nb_noms = 0
    noms = {}

    sites = chercheUtilisateurs(codefct=codefct)
    modele = re.compile('\W(A\d{4})\W')
    for s in sites:
        n = s[0]
        # bug dans chercheUtilisateurs !
        if not n.endswith(codefct):
            continue
        gecos = s[3]
        desc = Ldap.parseGecos(gecos)[0]
        code = modele.search(' ' + desc + ' ')
        if code != None:
            k = code.group(1)
            if k == None:
                k = 'E0000'
        else:
            k = 'A0000'

        if not k in noms:
            assos_noms += 1
            noms[k] = set()
        noms[k].add(n)
        nb_noms += 1
        
    # Retirer les pseudo-asso A0000 et E0000
    if 'A0000' in noms:
        assos_noms -= 1
    if 'E0000' in noms:
        assos_noms -= 1

    return (noms, 0, assos_noms, nb_noms)

def initDolib(dolikey:str, baseurl:str, codefct:str) -> (dict, int, int, int):
    '''Utilise l'API dolibarr pour récupérer la liste des clients
       Renvoie:
        - un dict A0123 -> [noms] 
        - un dict A0456 -> [noms] pour les sites de la mutu
        - Quelques stats
       Peut imprimer des warnings !'''

    headers = { 'Accept': 'application/json', 'DOLAPIKEY': dolikey }
    
    # On fait la liste des tiers, clients associations seulement
    url = baseurl + '/api/index.php/thirdparties'
    params = { 'sortfield':'t.code_client', 'sortorder': 'ASC', 'limit': 2000 }
    
    # On lance la requête pour voir la liste des clients
    req = requests.get(url, params=params, headers=headers)
    req.raise_for_status()
    clients = req.json()

    match codefct:
        case 'st':
            f_flag = 'options_site'
            f_nom = 'options_sitenom'
        case 'gt':
            f_flag = 'options_galette'
            f_nom = 'options_galnom'
        case 'db':
            f_flag = 'options_dolibarr'
            f_nom = 'options_dolibarrnom'
        case 'au':
            f_flag = 'options_autre'
            f_nom = 'options_nom'
        case _:
            raise Exception(f'ERREUR - codefct={codefct}')

    # On collecte qq infos sur les clients puis on les imprime
    dolib_assos = 0
    dolib_assos_noms = 0
    dolib_nb_noms = 0
    dolib_noms = {}
    dolib_mutu = {}

    for c in clients:
        if c['client'] == '1' and c['status'] == '1' and c['typent_code'] == 'TE_ASSADH':
            dolib_assos += 1
            if c['array_options'][f_flag] != None:
                dolib_assos_noms += 1
                try:
                    noms = sorted(c['array_options'][f_nom].split(','))
                    noms = [ n.strip() for n in noms ]
                    if c['array_options']['options_mutu'] != None:
                        dolib_mutu[c['code_client']] = noms
                    else: 
                        dolib_noms[c['code_client']] = noms
                    dolib_nb_noms += len(noms)
                except:
                    print (afficheJaune(f"ATTENTION - {c['code_client']} données incohérentes"))

    return (dolib_noms, dolib_mutu, dolib_assos, dolib_assos_noms, dolib_nb_noms)
    
# =============================== m a i n ===========================================
if '--help' in sys.argv:
    Usage()
    exit(0)

if len(sys.argv) != 3 or (not sys.argv[1] in ['verif'])or (not sys.argv[2] in [ 'st', 'au', 'gt','db']):
    Usage()
    exit(1)
else:
    CODEFCT = sys.argv[2]

try:
    # On passe qq paramètres par l'environnement
    DOLAPIKEY = os.getenv('PICDB_KEY')
    IFKEY = os.getenv('PICDB_IFKEY')
    BASEURL = os.getenv('PICDB_URL')
    DOMAINES = os.getenv("PICDB_DOMAINES") 
       
    if DOLAPIKEY == None:
        raise Exception("ERREUR - la variable d'environnement PICDB_KEY n'est pas définie !")
    if BASEURL == None:
        raise Exception("ERREUR - la variable d'environnement PICDB_URL n'est pas définie !")
    if os.getenv('PYTHONPATH') == None:
        raise Exception("ERREUR - la variable d'environnement PYTHONPATH n'est pas définie !")

    print("=======================================")
    print(" LES SITES HEBERGES")
    print("=======================================")
    
    # Serveur local: On vérifie les sites qui existent
    (local_noms, dum, local_assos_noms, local_nb_noms) = initSites(CODEFCT)
    printNoms(local_noms, CODEFCT)
    
    print ("=========================")
    print (f"NOMBRE D'ASSOS  = {local_assos_noms}")
    print (f"NOMBRE DE SITES = {local_nb_noms}" )

    print ()
    print("=======================================")
    print(" LES SITES DECLARES DANS DOLIBARR")    
    print("=======================================")

    # On lance la requête sur Dolibarr pour avoir la liste des noms de sites
    (dolib_noms, dolib_mutu, dolib_assos, dolib_assos_noms, dolib_nb_noms) = initDolib(dolikey=DOLAPIKEY, baseurl=BASEURL, codefct=CODEFCT)
    
    # On imprime la liste des code_client et des sites
    printNoms(dolib_noms, CODEFCT)

    print ()
    print("=============================================")
    print(" LES SITES DECLARES DANS DOLIBARR - MUTU SPIP")    
    print("=============================================")
    
    # On imprime la liste des code_client et des sites de la mutu
    printNoms(dolib_mutu)

    print()
    print ("============================================")
    print (f"NOMBRE D'ASSOS  = {dolib_assos_noms}")
    print (f"NOMBRE DE SITES = {dolib_nb_noms}" )
    print (f"NB TOTAL D'ASSOS= {dolib_assos}")
    print ("============================================")

    print()
    print("======================================================")
    print("LES SITES DECLARES DANS DOLIBARR mais NON EXISTANTS") 
    print("======================================================")
    printNoms(diffNoms(dolib_noms, local_noms), CODEFCT)
    
    print()
    print("====================================================")
    print(" LES SITES EXISTANTS MAIS NON DECLARES DANS DOLIBARR") 
    print("====================================================")
    printNoms(diffNoms(local_noms, dolib_noms), CODEFCT)

except KeyboardInterrupt:
    print(afficheRouge("\n\n===> Action interrompue par un CTRL-C"))
        
except HTTPError as he:
    print(afficheRouge(f"Erreur HTTP: {he}"))
    if 'PICDB_DEBUG' in os.environ:
        traceback.print_exception(e)

except Exception as e:
    print(afficheRouge(f"Erreur: {e}"))
    if 'PICDB_DEBUG' in os.environ:
        traceback.print_exception(e)
    
else:
    print("OK")

